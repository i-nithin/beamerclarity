const express = require("express");
const bodyParser = require("body-parser");
var cors = require("cors");
const app = express();
const port = process.env.PORT || 3000;
const clarity_decode = require("./clarity_decode/clarity.decode");

app.use(bodyParser.text());

app.use(cors());

let a = [];

app.get("/", (req, res) => {
  res.send("Hello World!");
});
// Script to embed into the site
{
  /* <script type="text/javascript">
    (function(c,l,a,r,i,t,y){
        c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
        t=l.createElement(r);t.async=1;t.src="http://localhost:3000/tag/"+i;
        y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);
    })(window, document, "clarity", "script", "l5554gerrp");
</script> */
}

app.get("/tag/:projectId", (req, res) => {
  const { projectId } = req.params;

  const script = `
      !function(c,l,a,r,i,t,y){
          if(a[c].v || a[c].t) return a[c]("event", c, "dup." + i.projectId);
          a[c].t = !0;
          (t = l.createElement(r)).async = !0;
          t.src = "https://clarity-min.onrender.com/clarity/clarity.min.js";
          (y = l.getElementsByTagName(r)[0]).parentNode.insertBefore(t, y);
          a[c]("start", i);
          a[c].q.unshift(a[c].q.pop());
          a[c]("set", "C_IS", "0");
      }("clarity", document, window, "script", {
          "projectId": "${projectId}",
          "upload": "https://beamerclarity.onrender.com/collect",
          "expire": 365,
          "cookies": ["_uetmsclkid", "_uetvid"],
          "track": true,
          "lean": false,
          "content": true,
          "dob": 1435
      });
  `;

  res.type("application/x-javascript").send(script);
});

app.post("/collect", (req, res) => {
  // console.log("Captured Request Body:", req.body);
  // console.log(clarity_decode.decode(req.body));
  // var decoded = clarity_decode.decode(req.body);
  a.push(req.body);
  res.status(204).send("Request body captured successfully!");
});

app.get("/collects", (req, res) => {
  if (a.length === 0) {
    console.log("failed");
    res.status(400).send("failed");
    return;
  }
  res.status(200).json(a);
    // a =[];
  console.log("success");
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
